//
//  HomepageSections.swift
//  DomainLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import Foundation

public enum HomepageSection: Int, CaseIterable {
    case heading
    case featureEvent
    case weeklyEvent
    case pokemonCard
    case erroneus
    
    public init(_ rawValue: Int) {
        self = HomepageSection(rawValue: rawValue) ?? .erroneus
    }
}

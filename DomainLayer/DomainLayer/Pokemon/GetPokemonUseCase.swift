//
//  GetPokemonUseCase.swift
//  DomainLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import class DataLayer.PokemonDAO
import protocol DataLayer.PokemonRepoInterface

public protocol GetPokemonUseCaseProtocol {
    func execute(offset:Int, completion: @escaping ([Pokemon]) -> ())
}

public final class GetPokemonUseCase: GetPokemonUseCaseProtocol {
    private let pokemonRepository: PokemonRepoInterface
    
    public init(pokemonRepository: PokemonRepoInterface) {
        self.pokemonRepository = pokemonRepository
    }
    
    public func execute(offset:Int, completion: @escaping ([Pokemon]) -> ()) {
        pokemonRepository.getPokemons(offset: offset) { result in
            switch result {
            case let .success(pokemonDAOs):
                completion(pokemonDAOs.map{ Pokemon(avatarImage: $0.base64Image, name: $0.name.capitalized)})
            case let .failure(error):
                print(error)
                completion([])
            }
        }
    }
}

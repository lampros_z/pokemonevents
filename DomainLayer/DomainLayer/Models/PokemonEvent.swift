//
//  PokemonEvent.swift
//  DomainLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import Foundation

public struct PokemonEvent {
    public let thumbnail: String
    public let name: String
    public let date: String
    public let venue: String
    
    public init(thumbnail: String, name: String, date: String, venue: String) {
        self.thumbnail = thumbnail
        self.name = name
        self.date = date
        self.venue = venue
    }
}

extension PokemonEvent {
    public static var mockData: [PokemonEvent] = [
        PokemonEvent(thumbnail: "Thumbnail1", name: "Legend? Go! Friends? Go!", date: "Sun 18 Oct", venue: "Cinnabar Island"),
        PokemonEvent(thumbnail: "Thumbnail2", name: "EXHALE with Professor Oak", date: "Sun 18 Oct", venue: "Celadon City"),
        PokemonEvent(thumbnail: "Thumbnail3", name: "Explore the seas", date: "Sun 18 Oct", venue: "Cerulean City"),
        PokemonEvent(thumbnail: "Thumbnail1", name: "Buing some pokeballs!", date: "Sun 19 Oct", venue: "Cinnabar Island"),
        PokemonEvent(thumbnail: "Thumbnail2", name: "Playing with Pikachu!", date: "Sun 18 Oct", venue: "Celadon City"),
        PokemonEvent(thumbnail: "Thumbnail3", name: "Race wars in the sea!", date: "Sun 18 Oct", venue: "Cerulean City"),
    ]
}

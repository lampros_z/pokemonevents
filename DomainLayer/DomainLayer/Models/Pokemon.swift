//
//  Pokemon.swift
//  DomainLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import Foundation

public struct Pokemon {
    public let avatarImage: String
    public let name: String
    
    public static let skeleton: Pokemon = Pokemon(avatarImage: "", name: "")
}

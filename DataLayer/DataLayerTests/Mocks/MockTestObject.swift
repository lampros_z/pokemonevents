//
//  MockTestObject.swift
//  DataLayerTests
//
//  Created by Lampros Zouloumis on 17/7/23.
//

import Foundation

struct MockTestObject: Codable {
    var property: String
}

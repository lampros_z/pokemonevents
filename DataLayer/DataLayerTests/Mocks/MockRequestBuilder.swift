//
//  MockRequestBuilder.swift
//  DataLayerTests
//
//  Created by Lampros Zouloumis on 17/7/23.
//

import Foundation
import Alamofire

enum MockRequestBuilder: URLRequestConvertible {
    case validUrl
    case invalidUrl
    
    func asURLRequest() throws -> URLRequest {
        var request = URLRequest(url: (try! path.asURL()))
        request.httpMethod = HTTPMethod.get.rawValue
        return request
    }
    
    private var path: String {
        switch self {
        case .validUrl:
            return "https://google.com"
        case .invalidUrl:
            return "htt://google.com"
        }
    }
}

//
//  MockStorage.swift
//  DataLayerTests
//
//  Created by Lampros Zouloumis on 17/7/23.
//

import Foundation
import RealmSwift
import Realm
@testable import DataLayer

public class MockStorage: BaseStorage {
}

public class MockPersistent: Object {
    @Persisted(primaryKey: true) var id = 0
    @Persisted  var property: String = ""
    
    convenience init(id: Int, property: String) {
        self.init()
        self.id = id
        self.property = property
    }
    
}

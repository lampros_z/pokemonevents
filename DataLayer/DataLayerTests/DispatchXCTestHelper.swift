//
//  DispatchXCTestHelper.swift
//  DataLayerTests
//
//  Created by Lampros Zouloumis on 17/7/23.
//

import Foundation
import XCTest

class DispatchXCTestHelper: XCTestCase {

    var expectation: XCTestExpectation?
    
    override func setUp() {
        super.setUp()
        expectation = expectation(description: "DispatchXCTestHelper")
    }
    
    func dispatch(action: @escaping () throws -> Void, completion: @escaping (_ error: Error?) -> Void) {
        var thrownError: Error?
        DispatchQueue.global(qos: .userInteractive).async {
            do {
                try action()
                self.expectation?.fulfill()
            } catch {
                thrownError = error
                self.expectation?.fulfill()
            }
        }
        
        wait(for: [expectation!], timeout: 1)
        completion(thrownError)
    }
}

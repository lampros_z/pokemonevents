//
//  FileReader.swift
//  DataLayerTests
//
//  Created by Lampros Zouloumis on 17/7/23.
//

import Foundation

public class FileReader {
    public static let shared = FileReader()
    private init() { }
    
    public func getData(file: String, ofType: String = "json") -> Data? {
        var bundle: Bundle!
        
        if let ebundle = Bundle(identifier: "com.lzouloumis.DataLayerTests") {
            bundle = ebundle
        } else {
            bundle = Bundle(for: FileReader.self)
        }
        
        guard let filePath = bundle.path(
            forResource: file,
            ofType: ofType
        ) else { return nil }

        let url = URL(fileURLWithPath: filePath)
        do {
            return try Data(contentsOf: url)
        } catch {
            return nil
        }
    }
}

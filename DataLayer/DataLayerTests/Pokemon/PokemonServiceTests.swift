//
//  PokemonServiceTests.swift
//  DataLayerTests
//
//  Created by Lampros Zouloumis on 17/7/23.
//


import XCTest
@testable import DataLayer

class PokemonServiceTests: DispatchXCTestHelper {
    
    var translator = PokemonTranslator()
    var service: PokemonService = .shared
    
    override func setUp() {
        super.setUp()
    }
    
    func testGetPokemonLinks() {
        var pokemonLinkDTO: PokemonLinkDTO?
        dispatch {
            pokemonLinkDTO = try self.service.getPokemonLinks(offset: 0)
        } completion: { thrownError in
            XCTAssertNotNil(pokemonLinkDTO)
            XCTAssertNil(thrownError)
        }
    }
    
    func testGetPokemon() {
        var pokemonDTOs: [PokemonDTO]?
        dispatch {
            pokemonDTOs = try self.service.getPokemons(names: ["bulbasaur", "ivysaur"])
        } completion: { thrownError in
            XCTAssertNotNil(pokemonDTOs)
            XCTAssertNil(thrownError)
        }
    }
    
}


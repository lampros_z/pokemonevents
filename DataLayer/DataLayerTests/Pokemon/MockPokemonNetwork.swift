//
//  MockPokemonNetwork.swift
//  DataLayerTests
//
//  Created by Lampros Zouloumis on 17/7/23.
//

import Foundation

public class MockPokemonNetwork: PokemonNetworkProtocol {
    
    public func getPokemonLinks(offset: Int) throws -> Data {
        let filename = "pokemon-links"
        return FileReader.shared.getData(file: filename)!
    }
    
    public func getPokemon(names: [String]) throws -> [Data] {
        let firstPokemon = "pokemon-1"
        let secondPokemon = "pokemon-2"
        var data = [Data]()
        data.append(FileReader.shared.getData(file: firstPokemon)!)
        data.append(FileReader.shared.getData(file: secondPokemon)!)
        return data
    }
    
}

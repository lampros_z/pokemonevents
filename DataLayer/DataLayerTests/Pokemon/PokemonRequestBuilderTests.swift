//
//  PokemonRequestBuilderTests.swift
//  DataLayerTests
//
//  Created by Lampros Zouloumis on 17/7/23.
//

import XCTest
import Alamofire
@testable import DataLayer

class PokemonRequestBuilderTests: XCTestCase {
    
    func testGetPokemonLinksEndpoint() {
        let builder = PokemonRequestBuilder.getPokemonLinks(offset: 0)
        guard let url = builder.urlRequest?.url else {
            XCTFail(); return
        }
        let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)
        XCTAssertEqual(urlComponents?.path, "/api/v2/pokemon")
    }
    
    func testGetPokemonEndpoint() {
        let pokemonName = "test"
        let builder = PokemonRequestBuilder.getPokemon(name: pokemonName)
        guard let url = builder.urlRequest?.url else {
            XCTFail(); return
        }
        let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)
        XCTAssertEqual(urlComponents?.path, "/api/v2/pokemon/\(pokemonName)")
    }
    
    func testGetPokemonLinksHttpMethod() {
        let builder = PokemonRequestBuilder.getPokemonLinks(offset: 0)
        guard let httpMethod = builder.urlRequest?.httpMethod else {
            XCTFail(); return
        }
        XCTAssertEqual(httpMethod, HTTPMethod.get.rawValue)
    }
    
    func testGetPokemonHttpMethod() {
        let pokemonName = "test"
        let builder = PokemonRequestBuilder.getPokemon(name: pokemonName)
        guard let httpMethod = builder.urlRequest?.httpMethod else {
            XCTFail(); return
        }
        XCTAssertEqual(httpMethod, HTTPMethod.get.rawValue)
    }
    
    
    func testGetPokemonLinksBaseUrl() {
        let builder = PokemonRequestBuilder.getPokemonLinks(offset: 0)
        guard let url = builder.urlRequest?.url else {
            XCTFail(); return
        }
        let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)
        guard let scheme = urlComponents?.scheme, let host = urlComponents?.host  else {
            XCTFail(); return
        }
        let baseUrlStringToTest = scheme + "://" + host
        XCTAssertEqual(baseUrlStringToTest, ProductionApi().baseUrl)
    }
    
    func testGetPokemonBaseUrl() {
        let pokemonName = "test"
        let builder = PokemonRequestBuilder.getPokemon(name: pokemonName)
        guard let url = builder.urlRequest?.url else {
            XCTFail(); return
        }
        let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)
        guard let scheme = urlComponents?.scheme, let host = urlComponents?.host  else {
            XCTFail(); return
        }
        let baseUrlStringToTest = scheme + "://" + host
        XCTAssertEqual(baseUrlStringToTest, ProductionApi().baseUrl)
    }
    
    
    func testGetPokemonLinksHeaders() {
        let builder = try? PokemonRequestBuilder.getPokemonLinks(offset: 0).asURLRequest()
        guard let headersDictionary = builder?.urlRequest?.headers.dictionary else {
            XCTFail(); return
        }
        var expectedHeaders = [String: String]()
        expectedHeaders["Accept"] = "application/json"
        XCTAssertEqual(headersDictionary, expectedHeaders)
    }
    
    func testGetPokemonHeaders() {
        let pokemonName = "test"
        let builder = try? PokemonRequestBuilder.getPokemon(name: pokemonName).asURLRequest()
        guard let headersDictionary = builder?.urlRequest?.headers.dictionary else {
            XCTFail(); return
        }
        
        var expectedHeaders = [String: String]()
        expectedHeaders["Accept"] = "application/json"
        XCTAssertEqual(headersDictionary, expectedHeaders)
    }
    
    func testGetPokemonLinksParameters() {
        let builder = try? PokemonRequestBuilder.getPokemonLinks(offset: 0).asURLRequest()
        guard let url = builder?.urlRequest?.url else {
            XCTFail(); return
        }
        let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)
        guard let urlComponents = urlComponents else {
            XCTFail(); return
        }
        guard let queryItems = urlComponents.queryItems else {
            XCTFail(); return
        }
        
        let limit = getQueryStringParameter(queryItems: queryItems, param: "limit")
        guard let limit = limit else {
            XCTFail(); return
        }
        XCTAssertEqual(Int(limit), 5)
        
        let offset = getQueryStringParameter(queryItems: queryItems, param: "offset")
        guard let offset = offset else {
            XCTFail(); return
        }
        XCTAssertEqual(Int(offset), 0)
    }

    func testGetPokemonParameters() {
        let builder = try? PokemonRequestBuilder.getPokemon(name: "test").asURLRequest()
        guard let url = builder?.urlRequest?.url else {
            XCTFail(); return
        }
        let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)
        guard let urlComponents = urlComponents else {
            XCTFail(); return
        }
        XCTAssertNil(urlComponents.queryItems)
    }
    
    
    private func getQueryStringParameter(queryItems: [URLQueryItem], param: String) -> String? {
        queryItems.first(where: { $0.name == param })?.value
    }
    
}


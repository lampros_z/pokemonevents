//
//  PokemonRepositoryTests.swift
//  DataLayerTests
//
//  Created by Lampros Zouloumis on 17/7/23.
//

import XCTest
@testable import DataLayer

class PokemonRepositoryTests: XCTestCase {
    var repo: PokemonRepoInterface!
    var expectation: XCTestExpectation!
    
    override func setUp() {
        repo = PokemonRepository()
        expectation = expectation(description: "PokemonRepository")
    }
    
    func testRetrieveWishlist() {
        var response: Result<[PokemonDAO], Error>?
        
        
        repo.getPokemons(offset: 0) { result in
            response = result
            self.expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1)
        
        switch response {
        case .success(_), .none: break
        case .failure(let error):
            XCTFail("Expected to have a success but got a failure: \(error)")
        }
    }
    
}


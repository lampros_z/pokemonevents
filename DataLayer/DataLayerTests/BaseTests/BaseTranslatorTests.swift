//
//  BaseTranslatorTests.swift
//  DataLayerTests
//
//  Created by Lampros Zouloumis on 17/7/23.
//

import XCTest
@testable import DataLayer

class BaseTranslatorTests: XCTestCase {
    private var translator: BaseTranslator?
    
    override func setUp() {
        self.translator = BaseTranslator()
    }
    
    func testValidTranslation() throws {
        let data = FileReader.shared.getData(file: "valid")!
        XCTAssertNoThrow(try translator?.translate(MockTestObject.self, data))
    }
    
    func testInvalidTranslation() throws {
        let data = FileReader.shared.getData(file: "invalid")!
        XCTAssertThrowsError(try translator?.translate(MockTestObject.self, data))
    }
}

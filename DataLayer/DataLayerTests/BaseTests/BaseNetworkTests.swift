//
//  BaseNetworkTests.swift
//  DataLayerTests
//
//  Created by Lampros Zouloumis on 17/7/23.
//

import XCTest
@testable import DataLayer

class BaseNetworkTests: XCTestCase {

    private var network: BaseNetwork?
    private var expectation: XCTestExpectation?
    
    override func setUpWithError() throws {
        network = BaseNetwork()
        expectation = expectation(description: "BaseNetwork")
    }

    func testSuccessfulResponse() throws {
        var catchedResponse: Data?
        
        DispatchQueue.global(qos: .userInteractive).async {
            catchedResponse = try? self.network?.request(MockRequestBuilder.validUrl)
            self.expectation?.fulfill()
        }

        waitForExpectations(timeout: 5) { error in
            XCTAssertNil(error)
            XCTAssertNotNil(catchedResponse)
        }
    }
    
    func testInvalidResponse() throws {
        var catchedResponse: Data?
        
        DispatchQueue.global(qos: .userInteractive).async {
            catchedResponse = try? self.network?.request(MockRequestBuilder.invalidUrl)
            self.expectation?.fulfill()
        }

        waitForExpectations(timeout: 5) { error in
            XCTAssertNil(catchedResponse)
        }
    }
}

//
//  BaseStorageTests.swift
//  DataLayerTests
//
//  Created by Lampros Zouloumis on 17/7/23.
//

import XCTest
@testable import DataLayer

class BaseStorageTests: XCTestCase {
    override func setUp() {
        
    }
    
    override class func tearDown() {
        let mockStorage = MockStorage()
        _ = try! mockStorage.delete(allOfType: MockPersistent.self)
    }
    
    func testGetTheNewlyCreatedObject() throws {
        let mockStorage = MockStorage()
        _ = try mockStorage.delete(allOfType: MockPersistent.self)
        let mockObject = MockPersistent(id: 1, property: "test1")
        _ = mockStorage.create(mockObject)
        let data = try mockStorage.get(MockPersistent.self)
        let results = mockStorage.translate(results: data)
        XCTAssertTrue(results[0] == mockObject)
    }
    
    func testCreateThenDeleteObject() throws {
        let mockStorage = MockStorage()
        _ = try mockStorage.delete(allOfType: MockPersistent.self)
        let mockObject = MockPersistent(id: 1, property: "test1")
        _ = mockStorage.create(mockObject)
        _ = try mockStorage.delete(mockObject)
        
        let data = try mockStorage.get(MockPersistent.self)
        let results = mockStorage.translate(results: data)
        XCTAssertTrue(results.isEmpty)
    }
    
    func testCreateTwoAndRefresh() throws {
        let mockStorage = MockStorage()
        _ = try mockStorage.delete(allOfType: MockPersistent.self)
        let mockObject = MockPersistent(id: 1, property: "test1")
        _ = mockStorage.create(mockObject)
        _ = mockStorage.create(MockPersistent(id: 2, property: "test2"))
        _ = mockStorage.refresh()
        
        let data = try mockStorage.get(MockPersistent.self)
        let results = mockStorage.translate(results: data)
        let count = try mockStorage.count(MockPersistent.self)
        XCTAssertTrue(results.count == count)
    }
    
    func testBatchCreate() throws {
        let mockStorage = MockStorage()
        _ = try mockStorage.delete(allOfType: MockPersistent.self)
        _ = mockStorage.createBatch([
            MockPersistent(id: 3, property: "test3"),
            MockPersistent(id: 4, property: "test4"),
            MockPersistent(id: 5, property: "test4")
        ])
        _ = mockStorage.refresh()
        let data = try mockStorage.get(MockPersistent.self)
        let results = mockStorage.translate(results: data)
        let count = try mockStorage.count(MockPersistent.self)
        XCTAssertTrue(results.count == count)
    }
    
}


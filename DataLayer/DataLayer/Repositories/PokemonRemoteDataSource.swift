//
//  PokemonRemoteDataSource.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import Foundation

public final class PokemonRemoteDataSource {
    
    func get(offset: Int) throws -> [PokemonDAO] {
        let pokemomLinks = try PokemonService.shared.getPokemonLinks(offset: offset)
        var pokemonDAOs = [PokemonDAO]()
        let pokemonNames = pokemomLinks.results.map { $0.name }
        let pokemonsDTOs = try PokemonService.shared.getPokemons(names: pokemonNames)
        let imageDownloads = ImageService.shared.downloadImagesConcurrently(imageUrls: pokemonsDTOs.map { $0.sprites.other.home.frontDefault } )
        pokemonDAOs.append(contentsOf: pokemonsDTOs
            .map { pokemonDTO in
                PokemonDAO(
                    id: pokemonDTO.id,
                    name: pokemonDTO.name,
                    base64Image: imageDownloads
                        .first(where: { imgUrl in
                            pokemonDTO
                                .sprites
                                .other
                                .home
                                .frontDefault == imgUrl
                                .url
                                .absoluteString
                        })?.image?.base64EncodedString() ?? "")
            }
            .sorted(by: {$0.id < $1.id})
        )
        print("--FETCHED: \(pokemonDAOs.count) pokemons--")
        return pokemonDAOs
    }
    
}

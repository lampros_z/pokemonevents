//
//  PokemonLocalDataSource.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 16/7/23.
//

import Foundation

public final class PokemonLocalDataSource {
    
    func getCount() throws -> Int {
        try PokemonStorage().getCount()
    }
    
    func get() throws -> [PokemonDAO] {
        let pokemon = try PokemonStorage().get()
        return pokemon.map { $0.toDAO() }
    }
    
    func store(pokemons: [PokemonDAO]) throws -> Bool {
        try PokemonStorage().save(pokemons.map { $0.toPersistent() })
    }
    
}

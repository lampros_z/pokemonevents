//
//  PokemonRepository.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import Foundation

public protocol PokemonRepoInterface {
    
    func getPokemons(
        offset: Int,
        completion: @escaping (Result<[PokemonDAO], Error>) -> ()
    )
    
}

public class PokemonRepository: PokemonRepoInterface {
    
    private let remoteDatasource: PokemonRemoteDataSource
    private let localDatasource: PokemonLocalDataSource
    
    public init() {
        self.remoteDatasource = PokemonRemoteDataSource()
        self.localDatasource = PokemonLocalDataSource()
    }
    
    public func getPokemons(offset: Int, completion: @escaping (Result<[PokemonDAO], Error>) -> ()) {
        DispatchQueue.global(qos: .userInteractive).async {
            do {
                var pokemonList = [PokemonDAO]()
                if try self.localDatasource.getCount() > offset {
                    let pokemons = try self.localDatasource.get()
                    pokemonList.append(contentsOf: pokemons)
                } else {
                    let pokemons = try self.remoteDatasource.get(offset: offset)
                    try self.sync(pokemons)
                    pokemonList.append(contentsOf: pokemons)
                }
                DispatchQueue.main.async {
                    completion(.success(pokemonList))
                }
            } catch {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
        }
    }
    
    private func sync(_ pokemons: [PokemonDAO]) throws {
        let result = try localDatasource.store(pokemons: pokemons)
        if !result { throw StorageError.failed }
        print("--SYNCED--")
    }
    
}

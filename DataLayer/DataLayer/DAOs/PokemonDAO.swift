//
//  PokemonDAO.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import Foundation

public class PokemonDAO {
    public let id: Int
    public let name: String
    public let base64Image: String
    
    init(id: Int, name: String, base64Image: String) {
        self.id = id
        self.name = name
        self.base64Image = base64Image
    }
}

extension PokemonDAO {
    func toPersistent() -> Pokemon {
        Pokemon(id: id, name: name, image: Data(base64Encoded: base64Image) ?? Data())
    }
}

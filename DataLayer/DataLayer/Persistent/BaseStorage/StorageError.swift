//
//  StorageError.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 16/7/23.
//

import Foundation

enum StorageError: Error {
    case failed
}

//
//  BaseStorage.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 16/7/23.
//

import Foundation
import RealmSwift
import Realm

public class BaseStorage {
    
    internal var storage: Realm?
    
    init() {
        var config = Realm.Configuration.defaultConfiguration
        config.fileURL!.deleteLastPathComponent()
        config.fileURL!.appendPathComponent(StorageSettings.pathComponent)
        config.fileURL!.appendPathExtension("realm")
        /*config.migrationBlock = { migration, oldSchemaVersion in
            if oldSchemaVersion < StorageSettings.schemaVersion {
                // Migration code
            }
        }*/
        self.storage = try? Realm(configuration: config)
        
    }
    
    func create<T: Object>(_ object: T) -> Bool {
        do {
            try storage?.write {
                storage?.add(object, update: .modified)
                storage?.refresh()
            }
            return true
        } catch {
            print(error)
            return false
        }
    }
    
    func createBatch<T: Object>(_ objects: [T]) -> Bool {
        do {
            try storage?.write {
                storage?.add(objects, update: .modified)
                storage?.refresh()
            }
            return true
        } catch {
            print(error)
            return false
        }
    }
    
    func count<T>(_ type: T.Type) throws -> Int where T: RealmFetchable {
        guard let count = storage?.objects(type.self).count else {
            throw StorageError.failed
        }
        return count
    }
    
    func get<T>(_ type: T.Type, key: Int) -> T? where T: RealmSwiftObject {
        return storage?.object(ofType: type.self, forPrimaryKey: key)
    }
    
    func get<T>(_ type: T.Type) throws -> Results<T> where T: RealmFetchable {
        guard let results = storage?.objects(type.self) else {
            throw StorageError.failed
        }
        
        return results
    }
    
    func translate<T>(results: Results<T>) -> [T] where T: RealmFetchable {
        var list = [T]()
        for result in results {
            list.append(result)
        }
        
        return list
    }
    
    func refresh() -> Bool {
        return (storage?.refresh())!
    }
    
    public func eraseAll() -> Bool {
        do {
            try storage?.write {
                storage?.deleteAll()
            }
            
            return true
        } catch {
            print(error)
            return false
        }
    }
    
    public func delete<T: Object>(_ object: T) throws -> Bool {
        try storage?.write {
            storage?.delete(object)
            storage?.refresh()
        }
        return true
    }
    
    public func delete<T: Object>(allOfType: T.Type) throws -> Bool {
        let results = try get(T.self)
        let total = results.count
        
        var totalDeleted = 0
        for item in results {
            if try delete(item) {
                totalDeleted += 1
            }
        }
        
        return totalDeleted == total
    }
    
}

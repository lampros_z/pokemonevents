//
//  PokemonStorage.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 16/7/23.
//

import Foundation

public class PokemonStorage: BaseStorage {
    
    public func getCount() throws -> Int {
        try count(Pokemon.self)
    }
    
    public func get() throws -> [Pokemon] {
        do {
            let data = try get(Pokemon.self)
            return translate(results: data)
        } catch {
            throw error
        }
    }
    
    public func save(_ pokemon: [Pokemon]) throws -> Bool {
        createBatch(pokemon)
    }
    
}

//
//  Pokemon.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 16/7/23.
//

import Foundation
import RealmSwift

public class Pokemon: Object {
    @Persisted(primaryKey: true) var id: Int = 0
    @Persisted var name: String = ""
    @Persisted var image: Data = Data()
    
    public convenience init(id: Int, name: String, image: Data) {
        self.init()
        self.id = id
        self.name = name
        self.image = image
    }
}

extension DataLayer.Pokemon {
    func toDAO() -> PokemonDAO {
        PokemonDAO(id: id, name: name, base64Image: image.base64EncodedString())
    }
}

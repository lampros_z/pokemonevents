//
//  BaseNetworkError.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import Foundation

enum NetworkError: Error {
    case notFound
    case badUrl
    case badFormat
    case failure(message: String)
    case failed(error: Error)
}

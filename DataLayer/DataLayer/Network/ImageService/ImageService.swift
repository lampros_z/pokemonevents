//
//  ImageService.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 16/7/23.
//

import Foundation

struct ImageDownload {
    let url: URL
    var image: Data?
}

public class ImageService {
    public static let shared = ImageService()
    private init() { }
    func downloadImagesConcurrently(imageUrls: [String]) -> [ImageDownload] {
        let maxConcurrentDownloads = 5
        let semaphore = DispatchSemaphore(value: maxConcurrentDownloads)
        let group = DispatchGroup()
        
        var downloads = imageUrls
            .compactMap { URL(string: $0) }
            .map { ImageDownload(url: $0, image: nil) }
        
        for (index, download) in downloads.enumerated() {
            group.enter()
            semaphore.wait()
            URLSession.shared.dataTask(with: download.url) { (data, response, error) in
                defer {
                    semaphore.signal()
                    group.leave()
                }
                if let data = data {
                    downloads[index].image = data
                }
            }.resume()
        }
        group.wait()
        return downloads
    }
}

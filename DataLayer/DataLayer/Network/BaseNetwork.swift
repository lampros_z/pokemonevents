//
//  BaseNetwork.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import Foundation
import Alamofire

internal class BaseNetwork {
    
    internal let session: Session!
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        session = Session(configuration: configuration)
        setCachePolicy()
    }
    
    internal func request(
        _ urlRequest: URLRequestConvertible,
        parameters: [String: Any]? = nil
    ) throws -> Data {
        var data: Data?
        var error: NetworkError?
        let semaphore = DispatchSemaphore(value: 0)
        setCachePolicy()
        
        session.request(urlRequest)
            .responseData { response in
                switch response.result {
                case let .success(jsonData):
                    data = jsonData
                case let .failure(errorResponse):
                    error = NetworkError.failed(error: errorResponse)
                }
                semaphore.signal()
            }
        
        _ = semaphore.wait(timeout: .distantFuture)
        guard let data = data, error == nil else {
            throw error ?? NetworkError.failure(message: "Uknown error occured.")
        }
        return data
    }
    
    internal func concurrentRequests(
        _ urlRequests: [URLRequestConvertible],
        maxConcurrentRequests: Int,
        parameters: [String: Any]? = nil
    ) throws -> [Data] {
        var data: [Data] = []
        var errors: [NetworkError] = []
        let semaphore = DispatchSemaphore(value: maxConcurrentRequests)
        let group = DispatchGroup()
        setCachePolicy()
        for request in urlRequests {
            group.enter()
            semaphore.wait()
            session.request(request)
                .responseData { response in
                    switch response.result {
                    case let .success(jsonData):
                        data.append(jsonData)
                    case let .failure(errorResponse):
                        errors.append(NetworkError.failed(error: errorResponse))
                    }
                    semaphore.signal()
                    group.leave()
                }
        }

        group.wait()
        guard !data.isEmpty, errors.isEmpty else {
            throw errors.first ?? NetworkError.failure(message: "Uknown error occured.")
        }
        return data
    }
    
    
    private func setCachePolicy() {
        session.sessionConfiguration.requestCachePolicy = .reloadIgnoringLocalCacheData
    }
    
}

//
//  BaseApi.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import Foundation

public protocol BaseApiProtocol {
    var baseUrl: String { get }
}

public class ProductionApi: BaseApiProtocol {
    public var baseUrl: String {
        return "https://pokeapi.co"
    }
}

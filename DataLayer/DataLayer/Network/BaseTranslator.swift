//
//  BaseTranslator.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import Foundation

public class BaseTranslator {
    
    public func translate<T>(_ type: T.Type, _ data: Data) throws -> T where T: Decodable {
        let result: T
        do {
            result = try JSONDecoder().decode(type, from: data)
        } catch {
            print("--ERROR--\(#function)--\(error)")
            throw error
        }
        return result
    }
    
}

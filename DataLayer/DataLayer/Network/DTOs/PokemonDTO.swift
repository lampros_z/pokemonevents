//
//  PokemonDTO.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import Foundation

public struct PokemonDTO: Codable {
    public let id: Int
    public let name: String
    public let sprites: SpriteDTO
}

public struct SpriteDTO: Codable {
    public let other: OtherDTO
}

public struct OtherDTO: Codable {
    public let home: HomeDTO
}

public struct HomeDTO: Codable {
    public let frontDefault: String
    
    public enum CodingKeys: String, CodingKey {
          case frontDefault = "front_default"
    }
}

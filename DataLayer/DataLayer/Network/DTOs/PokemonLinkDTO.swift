//
//  PokemonLinkDTO.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import Foundation

public struct PokemonLinkDTO: Codable {
    public let results: [PokemonResultsDTO]
}

public struct PokemonResultsDTO: Codable {
    public let name: String
    public let url: String
}

//
//  PokemonService.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import Foundation

public class PokemonService {
    
    public static let shared = PokemonService()
    
    private let network: PokemonNetworkProtocol
    private let translator: PokemonTranslator
    
    public init() {
        if ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil {
            self.network = MockPokemonNetwork()
        } else {
            self.network = PokemonNetwork()
        }
        self.translator = PokemonTranslator()
    }
    
    public func getPokemonLinks(offset: Int) throws -> PokemonLinkDTO {
        let data = try network.getPokemonLinks(offset: offset)
        return try translator.trnalsatePokemonLinks(data)
    }
    
    public func getPokemons(names: [String]) throws -> [PokemonDTO] {
        let data = try network.getPokemon(names: names)
        return try translator.translatePokemon(data)
    }
}

//
//  PokemonRequestBuilder.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import Foundation
import Alamofire

public enum PokemonRequestBuilder: URLRequestConvertible {
    case getPokemonLinks(offset: Int)
    case getPokemon(name: String)
    
    private var httpMethod: HTTPMethod {
        switch self {
        case .getPokemon, .getPokemonLinks:
            return .get
        }
    }
    
    private var baseUrl: URL {
        switch self {
        default:
            return try! ProductionApi().baseUrl.asURL()
        }
    }
    
    private var path: String {
        switch self {
        case .getPokemonLinks:
            return "/api/v2/pokemon"
        case .getPokemon(let name):
            return "/api/v2/pokemon/\(name)"
        }
    }
    
    private var parameters: [String: Any] {
        switch self {
        case let .getPokemonLinks(offset):
            return [
                "limit": 5,
                "offset": offset
            ]
        case .getPokemon:
            return [:]
        }
    }
    
    private var headers: [String: String] {
        var httpHeaders = [String: String]()
        httpHeaders["Accept"] = "application/json"
        return httpHeaders
    }
    
    public func asURLRequest() throws -> URLRequest {
        var request = URLRequest(url: baseUrl.appendingPathComponent(path))

        request.httpMethod = httpMethod.rawValue
        
        switch self {
        case .getPokemonLinks, .getPokemon:
            request = try URLEncoding.default.encode(request, with: parameters)
        }
        
        
        for header in headers {
            request.setValue(
                header.value,
                forHTTPHeaderField: header.key
            )
        }

        return request
    }
}

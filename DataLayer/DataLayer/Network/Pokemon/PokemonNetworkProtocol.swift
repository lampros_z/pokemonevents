//
//  PokemonNetworkProtocol.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 17/7/23.
//

import Foundation

protocol PokemonNetworkProtocol {
    func getPokemonLinks(offset: Int) throws -> Data
    func getPokemon(names: [String]) throws -> [Data]
}

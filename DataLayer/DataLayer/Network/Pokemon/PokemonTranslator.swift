//
//  PokemonTranslator.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import Foundation

public class PokemonTranslator: BaseTranslator {
    
    public func trnalsatePokemonLinks(_ data: Data) throws -> PokemonLinkDTO {
        try translate(PokemonLinkDTO.self, data)
    }
    
    public func translatePokemon(_ data: [Data]) throws -> [PokemonDTO] {
        data.compactMap{ data in try? translate(PokemonDTO.self, data) }
    }
    
}

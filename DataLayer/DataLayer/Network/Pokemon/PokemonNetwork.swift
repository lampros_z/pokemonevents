//
//  PokemonNetwork.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import Foundation

class PokemonNetwork: BaseNetwork, PokemonNetworkProtocol {
    func getPokemonLinks(offset: Int) throws -> Data {
        try request(PokemonRequestBuilder.getPokemonLinks(offset: offset))
    }
    
    func getPokemon(names: [String]) throws -> [Data] {
        let requests = names.compactMap { try? PokemonRequestBuilder.getPokemon(name: $0).asURLRequest()
        }
        let data = try concurrentRequests(requests, maxConcurrentRequests: 5)
        return data
    }
}

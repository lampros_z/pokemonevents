//
//  StorageSettings.swift
//  DataLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import Foundation

public struct StorageSettings {
    static let pathComponent = "pokemons"
    static let schemaVersion: UInt64 = 1
}

//
//  HomepageFlowAssembler.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import DomainLayer
import DataLayer
import Swinject

public protocol HomepageFlowAssemblerProtocol {
    func build() -> HomepageCoordinatorProtocol
}


public class HomepageFlowAssembler: HomepageFlowAssemblerProtocol {
    private let assembler: Assembler
    
    public init() {
        assembler = Assembler([HomepageFlowContainer()], parent: nil)
    }
    
    public func build() -> HomepageCoordinatorProtocol {
        assembler.resolver.resolve(HomepageCoordinatorProtocol.self)!
    }
}

public class HomepageFlowContainer: Assembly {
    
    public init() { }
    
    public func assemble(container: Container) {
        
        container.register(HomepageCoordinatorProtocol.self) { r in
            let homepageViewModel = r.resolve(HomepageViewModel.self)!
            return HomepageCoordinator(homepageViewController: HomepageViewController(viewModel: homepageViewModel))
        }

        
        container.register(HomepageViewModel.self) { r in
            let getPokemonUseCase = r.resolve(GetPokemonUseCaseProtocol.self)!
            return HomepageViewModel(getPokemonUseCase: getPokemonUseCase)
        }
        .initCompleted { r, homepageViewModel in
            homepageViewModel.coordinatorDelegate = (r.resolve(HomepageCoordinatorProtocol.self)! as? HomepageCoordinator)
        }
        
        container.register(GetPokemonUseCaseProtocol.self) { r in
            let pokemonRepository = r.resolve(PokemonRepoInterface.self)!
            return GetPokemonUseCase(pokemonRepository: pokemonRepository)
        }
        
        container.register(PokemonRepoInterface.self) { _ in
            PokemonRepository()
        }
    }
    
}

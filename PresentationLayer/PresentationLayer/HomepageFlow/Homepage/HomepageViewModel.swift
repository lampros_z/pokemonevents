//
//  HomepageViewModel.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//


import UIKit
import DomainLayer

public protocol HomepageCoordinatorDelegate: AnyObject {
    func navigateToAuthenticationScreen()
    func navigateToConnectionScreen()
}

public class HomepageViewModel {
    
    var coordinatorDelegate: HomepageCoordinatorDelegate?
    public let maxSkeletonCells = 5
    private let getPokemonUseCase: GetPokemonUseCaseProtocol
    
    init(getPokemonUseCase: GetPokemonUseCaseProtocol) {
        self.getPokemonUseCase = getPokemonUseCase
    }
    
    public var weeklyEvents: [PokemonEvent] = []
    public var pokemons: [Pokemon] = []
    public var skeletonPokemons: [Pokemon] = []
    private var tempPokemon: [Pokemon] = []
    
    private var fetchingPokemon = false
    
    public func sectionReuseIdentifiers(indexPath: IndexPath) -> String {
        let reuseIdentifiers = [
            HeadingCell.reuseIdentifier,
            FeaturedEventCell.reuseIdentifier,
            WeeklyEventCell.reuseIdentifier,
            PokemonCard.reuseIdentifier
        ]
        return reuseIdentifiers[indexPath.section]
    }
    
    
    public func fetchEvents(completion: @escaping () -> ()) {
        self.weeklyEvents.append(contentsOf: PokemonEvent.mockData)
        completion()
    }
    
    public func addSkeleton(skeletonHandler: @escaping () -> ()) {
        guard !fetchingPokemon else { return }
        fetchingPokemon = true
        skeletonPokemons.append(contentsOf: repeatElement(Pokemon.skeleton, count: maxSkeletonCells))
        skeletonHandler()
    }
    
    public func fetchPokemon(completion: @escaping () -> ()) {
        getPokemonUseCase.execute(offset: pokemons.count) { pokemons in
            self.tempPokemon.append(contentsOf: pokemons)
            self.skeletonPokemons.removeAll(keepingCapacity: true)
            completion()
        }
    }
    
    public func onRemovedSkeleton(completion: @escaping () -> ()) {
        pokemons.append(contentsOf: tempPokemon)
        tempPokemon.removeAll(keepingCapacity: true)
        completion()
    }
    
    public func onCompleted() {
        self.fetchingPokemon = false
    }
    
    
    public func numberOfSections() -> Int {
        return HomepageSection.allCases.count - 1
    }
    
    public func numberOfItemsInSection(_ section: Int) -> Int {
        let section = HomepageSection(section)
        switch section {
        case .heading, .featureEvent:
            return 1
        case .weeklyEvent:
            return weeklyEvents.count
        case .pokemonCard:
            return pokemons.count + skeletonPokemons.count
        default:
            return 0
        }
    }
    
    public func configureSupplementaryView(_ view: UICollectionReusableView, indexPath: IndexPath) {
        let section = HomepageSection(indexPath.section)
        switch section {
        case .weeklyEvent:
            (view as? CollectionGenericHeader)?.configureHeader(title: "This week")
        case .pokemonCard:
            (view as? CollectionGenericHeader)?.configureHeader(title: "Popular Pokemon")
        default:
            return
        }
    }
    
    public func configureCell(cell: UICollectionViewCell, indexPath: IndexPath) {
        let configurations: [(UICollectionViewCell, Int) -> Void] = [
            { (cell, row) in (cell as? HeadingCell)?.delegate = self },
            { (cell, row) in (cell as? FeaturedEventCell)?.configure(image: ImageCatalog.eventThumbnail, subtitle: "Popular in Kanto", eventName: "The Dream Continues!", date: "Tue 4 Oct", venue: "Pallet Town") },
            { (cell, row) in (cell as? WeeklyEventCell)?.configure(with: self.weeklyEvents[row])},
            { (cell, row) in
                guard row < self.pokemons.count else {return}
                (cell as? PokemonCard)?.configure(pokemon: self.pokemons[row])
                (cell as? PokemonCard)?.delegate = self
            }
        ]
        guard indexPath.section < configurations.count, indexPath.section < HomepageSection.allCases.count - 1 else { return }
        configurations[indexPath.section](cell, indexPath.row)
    }
}

extension HomepageViewModel: HeadingCellDelegate, PokemonCardDelegate {
    func didTapConnectButton() {
        coordinatorDelegate?.navigateToConnectionScreen()
    }
    
    func didTapAuthenticationButton() {
        coordinatorDelegate?.navigateToAuthenticationScreen()
    }
}

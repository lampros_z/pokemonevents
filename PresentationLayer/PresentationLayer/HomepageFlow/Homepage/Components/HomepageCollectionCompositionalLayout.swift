//
//  HomepageCollectionCompositionalLayout.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import enum DomainLayer.HomepageSection
import UIKit

public class HomepageCollectionCompositionalLayout {
    
    public func build(onInvalidation: @escaping ([NSCollectionLayoutVisibleItem], CGPoint, NSCollectionLayoutEnvironment) -> ()) -> UICollectionViewCompositionalLayout {
        
        let itemsForSections = [
            NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .estimated(40))),
            NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .estimated(430))),
            NSCollectionLayoutItem(layoutSize: .init(widthDimension: .absolute(186), heightDimension: .estimated(244))),
            NSCollectionLayoutItem(layoutSize: .init(widthDimension: .absolute(138), heightDimension: .estimated(196)))
        ]
        
        let contentInsetsForSections = [
            NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0),
            NSDirectionalEdgeInsets(top: 28, leading: 0, bottom: 40, trailing: 0),
            NSDirectionalEdgeInsets(top: 0, leading: 15.5, bottom: 40, trailing: 0),
            NSDirectionalEdgeInsets(top: 8, leading: 16, bottom: 24, trailing: 0),
        ]
        
        let groupWidthsForSection = [
            NSCollectionLayoutDimension.fractionalWidth(1),
            NSCollectionLayoutDimension.fractionalWidth(1),
            NSCollectionLayoutDimension.absolute(168),
            NSCollectionLayoutDimension.absolute(138)
        ]
        
        let groupEdgeSpacing = [
            NSCollectionLayoutEdgeSpacing(leading: .none, top: .none, trailing: .none, bottom: .none),
            NSCollectionLayoutEdgeSpacing(leading: .none, top: .none, trailing: .none, bottom: .none),
            NSCollectionLayoutEdgeSpacing(leading: .none, top: .none, trailing: .fixed(8), bottom: .none),
            NSCollectionLayoutEdgeSpacing(leading: .none, top: .none, trailing: .fixed(8), bottom: .none)
        ]
        
        let layout = UICollectionViewCompositionalLayout {
            (sectionIndex: Int, layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
            let item = itemsForSections[sectionIndex]
            let groupHeight = NSCollectionLayoutDimension.estimated(item.layoutSize.heightDimension.dimension)
            let groupSize = NSCollectionLayoutSize(widthDimension: groupWidthsForSection[sectionIndex], heightDimension: groupHeight)
            let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 1)
            group.edgeSpacing = groupEdgeSpacing[sectionIndex]
            let section = NSCollectionLayoutSection(group: group)
            if sectionIndex > 1  {
                section.boundarySupplementaryItems = [
                    .init(
                        layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .absolute(24)),
                        elementKind: UICollectionView.elementKindSectionHeader,
                        alignment: .topLeading
                    )
                ]
            }
            section.orthogonalScrollingBehavior = .continuous
            section.contentInsets = contentInsetsForSections[sectionIndex]
            section.visibleItemsInvalidationHandler = { items, point ,env in
                onInvalidation(items, point, env)
            }
            return section
        }
        return layout
    }
    
}

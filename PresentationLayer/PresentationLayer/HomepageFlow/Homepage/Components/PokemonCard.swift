//
//  PokemonCard.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import UIKit
import struct DomainLayer.Pokemon
import SkeletonView

protocol PokemonCardDelegate: AnyObject {
    func didTapConnectButton()
}

public class SkeletonCard: UICollectionViewCell {
    static let reuseIdentifier = "PokemonEvents.SkeletonCard"
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        isSkeletonable = true
        skeletonCornerRadius = 4
        layer.cornerRadius = 4
        showGradientSkeleton(usingGradient: SkeletonGradient.init(baseColor: ColorPalette.contentLightGrey),transition: .crossDissolve(0.8))
        startSkeletonAnimation(SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .topLeftBottomRight))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

public class PokemonCard: UICollectionViewCell {
    static let reuseIdentifier = "PokemonEvents.PokemonCard"
    weak var delegate: PokemonCardDelegate?
    private let svgWebview: SVGWebview = {
        let svgWebview = SVGWebview()
        svgWebview.translatesAutoresizingMaskIntoConstraints = false
        return svgWebview
    }()
    
    private lazy var svgWebviewConstraints: [NSLayoutConstraint] = [
        svgWebview.heightAnchor.constraint(equalToConstant: 82),
        svgWebview.widthAnchor.constraint(equalToConstant: 82),
        svgWebview.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
        svgWebview.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
    ]
    
    private let pokemonName: UILabel = {
        let label = UILabel()
        label.font = Fonts.sfproRegularFourteen
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var pokemonNameConstrains: [NSLayoutConstraint] = [
        pokemonName.topAnchor.constraint(equalTo: svgWebview.bottomAnchor, constant: 8),
        pokemonName.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
        pokemonName.heightAnchor.constraint(equalToConstant: 36)
    ]
    
    private let connectButton: CustomButton = {
        let button = CustomButton(title: "CONNECT", font: Fonts.sfproBoldTwelve)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var connectButtonConstraints: [NSLayoutConstraint] = [
        connectButton.topAnchor.constraint(equalTo: pokemonName.bottomAnchor, constant: 8),
        connectButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
        connectButton.heightAnchor.constraint(equalToConstant: 32)
    ]
    
    private let spacer: UIView = {
       let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var spacerConstraints: [NSLayoutConstraint] = [
        spacer.topAnchor.constraint(equalTo: connectButton.bottomAnchor),
        spacer.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
        spacer.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
        spacer.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
    ]
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        configureAppearance()
        addSubviewsToHierarchy()
        setupLayout()
        configureConnectButton()
    }
    
    public func configure(pokemon: Pokemon) {
        svgWebview.configure()
        svgWebview.loadSVG(base64png: pokemon.avatarImage)
        pokemonName.text = pokemon.name
    }
    
    private func configureAppearance() {
        backgroundColor = ColorPalette.backgroundSurfaceGrey
        layer.cornerRadius  = 4
    }
    
    private func addSubviewsToHierarchy() {
        contentView.addSubview(svgWebview)
        contentView.addSubview(pokemonName)
        contentView.addSubview(connectButton)
        contentView.addSubview(spacer)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate(
            svgWebviewConstraints + pokemonNameConstrains + connectButtonConstraints + spacerConstraints
        )
    }
    
    private func configureConnectButton() {
        connectButton.addTarget(self, action: #selector(didTapConnectButton), for: .touchUpInside)
    }
    
    @objc private func didTapConnectButton() {
        delegate?.didTapConnectButton()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  HeadingCell.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 13/7/23.
//

import UIKit

protocol HeadingCellDelegate: AnyObject {
    func didTapAuthenticationButton()
}

public class HeadingCell: UICollectionViewCell {
    static let reuseIdentifier = "PoekmonEvents.HeadingCell"
    weak var delegate: HeadingCellDelegate?
    private lazy var container: UIStackView = {
       let stackView = UIStackView(arrangedSubviews: [locationView, HorizontalSpacer(width: 16), authenticationButton])
        stackView.axis = .horizontal
        stackView.spacing = 0
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var containerConstrains: [NSLayoutConstraint] = [
        container.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
        container.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
        container.topAnchor.constraint(equalTo: contentView.topAnchor),
        container.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
    ]
    
    private let locationView: LocationView = {
       let view = LocationView()
        view.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        return view
    }()
    
    private let authenticationButton: CustomButton = {
        let button = CustomButton(title: "Sign In or Register", font: Fonts.sfproMediumSixteen)
        button.setContentHuggingPriority(.defaultLow, for: .horizontal)
        button.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return button
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        addChildViewsToHierarchy()
        setupLayout()
        configureViews()
    }
    
    private func addChildViewsToHierarchy() {
        contentView.addSubview(container)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate( containerConstrains )
    }
    
    private func configureViews() {
        locationView.configureLoction("Kanto")
        authenticationButton.addTarget(self, action: #selector(didTapAuthButton), for: .touchUpInside)
    }
    
    @objc private func didTapAuthButton() {
        delegate?.didTapAuthenticationButton()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

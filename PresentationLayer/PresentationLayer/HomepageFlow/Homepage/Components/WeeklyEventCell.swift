//
//  WeeklyEventCell.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import UIKit
import struct DomainLayer.PokemonEvent

public class WeeklyEventCell: UICollectionViewCell {
    public static let reuseIdentifier = "PokemonEvents.WeeklyEventCell"
    
    private let eventThumbnail: UIImageView = {
       let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var eventThumbnailConstraints: [NSLayoutConstraint] = [
        eventThumbnail.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
        eventThumbnail.topAnchor.constraint(equalTo: contentView.topAnchor),
        eventThumbnail.heightAnchor.constraint(equalToConstant: 168),
        eventThumbnail.widthAnchor.constraint(equalToConstant: 168)
    ]
    
    private let eventName: UILabel = {
       let label = UILabel ()
        label.font = Fonts.sfproRegularFourteen
        label.textColor = .white
        label.numberOfLines = 0
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var eventNameLabelContraints: [NSLayoutConstraint] = [
        eventName.leadingAnchor.constraint(equalTo: eventThumbnail.leadingAnchor),
        eventName.trailingAnchor.constraint(equalTo: eventThumbnail.trailingAnchor),
        eventName.topAnchor.constraint(equalTo: eventThumbnail.bottomAnchor, constant: 8)
    ]
    
    private let date: UILabel = {
       let label = UILabel ()
        label.font = Fonts.sfproRegularFourteen
        label.textColor = ColorPalette.contentLightGrey
        label.numberOfLines = 1
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var dateLabelConstraints: [NSLayoutConstraint] = [
        date.leadingAnchor.constraint(equalTo: eventThumbnail.leadingAnchor),
        date.trailingAnchor.constraint(equalTo: eventThumbnail.trailingAnchor),
        date.topAnchor.constraint(equalTo: eventName.bottomAnchor)
    ]
    
    private let venue: UILabel = {
       let label = UILabel ()
        label.font = Fonts.sfproRegularFourteen
        label.textColor = ColorPalette.contentLightGrey
        label.numberOfLines = 1
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var venuLabelConstraints: [NSLayoutConstraint] = [
        venue.leadingAnchor.constraint(equalTo: eventThumbnail.leadingAnchor),
        venue.trailingAnchor.constraint(equalTo: eventThumbnail.trailingAnchor),
        venue.topAnchor.constraint(equalTo: date.bottomAnchor),
        venue.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
    ]
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        configureAppearance()
        addChildViewsToHierarchy()
        setupLayout()
    }
    
    public func configure(with pokemonEvent: PokemonEvent) {
        eventThumbnail.image = UIImage(named: pokemonEvent.thumbnail)
        eventName.text = pokemonEvent.name
        date.text = pokemonEvent.date
        venue.text = pokemonEvent.venue
    }
    
    private func configureAppearance() {
        backgroundColor = .clear
    }
    
    private func addChildViewsToHierarchy() {
        contentView.addSubview(eventThumbnail)
        contentView.addSubview(eventName)
        contentView.addSubview(date)
        contentView.addSubview(venue)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate(
            eventThumbnailConstraints +
            eventNameLabelContraints +
            dateLabelConstraints +
            venuLabelConstraints
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

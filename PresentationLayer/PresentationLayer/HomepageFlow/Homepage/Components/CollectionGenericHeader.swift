//
//  CollectionGenericHeader.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import UIKit

public class CollectionGenericHeader: UICollectionReusableView {
    static let reuseIdentifier: String = "PokemonEvents.CollectionGenericHeader"
    
    private let label: UILabel = {
       let label = UILabel()
        label.textColor = .white
        label.font = Fonts.sfproRegularEighteen
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(label)
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            label.trailingAnchor.constraint(equalTo: trailingAnchor),
            label.topAnchor.constraint(equalTo: topAnchor),
            label.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    public func configureHeader(title: String) {
        label.text = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

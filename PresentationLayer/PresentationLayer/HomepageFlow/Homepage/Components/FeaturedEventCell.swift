//
//  FeaturedEventCell.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 14/7/23.
//

import UIKit

public class FeaturedEventCell: UICollectionViewCell {
    
    static let reuseIdentifier = "PokemonEvents.FeaturedEventCell"
    
    private let eventThumnail: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var eventThumnailConstraints: [NSLayoutConstraint] = [
        eventThumnail.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
        eventThumnail.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
        eventThumnail.topAnchor.constraint(equalTo: topAnchor)
    ]
    
    private let subtitleLabel: UILabel = {
       let label = UILabel()
        label.textAlignment = .left
        label.font = Fonts.sfproRegularEighteen
        label.textColor = ColorPalette.contentAcid
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var subtitleLabelConstraints: [NSLayoutConstraint] = [
        subtitleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
        subtitleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
        subtitleLabel.topAnchor.constraint(equalTo: eventThumnail.bottomAnchor, constant: 16)
    ]
    
    private let eventNameLabel: UILabel = {
       let label = UILabel()
        label.textAlignment = .left
        label.font = Fonts.sfproRegularTwentyTwo
        label.textColor = .white
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var eventNameLabelConstraints: [NSLayoutConstraint] = [
        eventNameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
        eventNameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
        eventNameLabel.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor)
    ]
    
    private let dateLabel: UILabel = {
       let label = UILabel()
        label.textAlignment = .left
        label.font = Fonts.sfproRegularFourteen
        label.textColor = ColorPalette.contentLightGrey
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        return label
    }()
    
    private lazy var dateLabelConstraints: [NSLayoutConstraint] = [
        dateLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
        dateLabel.topAnchor.constraint(equalTo: eventNameLabel.bottomAnchor)
    ]
    
    private let venueLabel: UILabel = {
       let label = UILabel()
        label.textAlignment = .left
        label.font = Fonts.sfproRegularFourteen
        label.textColor = ColorPalette.contentLightGrey
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var venueLabelConstraints: [NSLayoutConstraint] = [
        venueLabel.leadingAnchor.constraint(equalTo: dateLabel.trailingAnchor, constant: 16),
        venueLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
        venueLabel.topAnchor.constraint(equalTo: eventNameLabel.bottomAnchor),
        venueLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
    ]
    
    public func configure(
        image: UIImage?,
        subtitle: String,
        eventName: String,
        date: String,
        venue: String
    ) {
        eventThumnail.image = image
        subtitleLabel.text = subtitle
        eventNameLabel.text = eventName
        dateLabel.text = date
        venueLabel.text = venue
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        addChildViewsToHierarchy()
        setupLayout()
    }
    
    private func addChildViewsToHierarchy() {
        contentView.addSubview(eventThumnail)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(eventNameLabel)
        contentView.addSubview(dateLabel)
        contentView.addSubview(venueLabel)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate(
            eventThumnailConstraints +
            subtitleLabelConstraints +
            eventNameLabelConstraints +
            dateLabelConstraints +
            venueLabelConstraints
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

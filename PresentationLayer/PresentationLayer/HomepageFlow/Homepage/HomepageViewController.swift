//
//  HomepageViewController.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import UIKit
import DomainLayer
import SkeletonView

public class HomepageViewController: BaseViewController {
    
    let viewModel: HomepageViewModel
    let homepageLayout =  HomepageCollectionCompositionalLayout()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: homepageLayout.build(onInvalidation: self.onInvalidation(_:_:_:)))
        collectionView.register(HeadingCell.self, forCellWithReuseIdentifier: HeadingCell.reuseIdentifier)
        collectionView.register(FeaturedEventCell.self, forCellWithReuseIdentifier: FeaturedEventCell.reuseIdentifier)
        collectionView.register(WeeklyEventCell.self, forCellWithReuseIdentifier: WeeklyEventCell.reuseIdentifier)
        collectionView.register(PokemonCard.self, forCellWithReuseIdentifier: PokemonCard.reuseIdentifier)
        collectionView.register(SkeletonCard.self, forCellWithReuseIdentifier: SkeletonCard.reuseIdentifier)
        collectionView.register(CollectionGenericHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: CollectionGenericHeader.reuseIdentifier)
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    private lazy var collectionViewConstraints: [NSLayoutConstraint] = [
        collectionView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12),
        collectionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
        collectionView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
        collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
    ]
    
    public init(viewModel: HomepageViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        addChildViewsToHierarchy()
        setupLayout()
        subscribeToDelegates()
        viewModel.fetchEvents {
            self.collectionView.reloadSections([HomepageSection.weeklyEvent.rawValue])
        }
        fetchPokemon()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { context in
            self.collectionView.collectionViewLayout.invalidateLayout()
        }, completion: nil)
    }
    
    private func addChildViewsToHierarchy() {
        contentView.addSubview(collectionView)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate( collectionViewConstraints )
    }
    
    private func subscribeToDelegates() {
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    private var currentPoint = CGFloat(0)
    private func onInvalidation(_ items: [NSCollectionLayoutVisibleItem], _ point: CGPoint, _ environemnt: NSCollectionLayoutEnvironment) {
        guard currentPoint < point.x else {
            currentPoint = point.x
            return
        }
        currentPoint = point.x
        if items.allSatisfy({ $0.indexPath.section == HomepageSection.pokemonCard.rawValue }) {
            if (items.last?.indexPath.row ?? 0) >= (viewModel.pokemons.count - 1)  {
                fetchPokemon()
            }
        }
    }
    
    
    private func fetchPokemon() {
        var insertionPaths = [IndexPath]()
        viewModel.addSkeleton {
            let sectionUpdates = {
                let c = self.collectionView.numberOfItems(inSection: HomepageSection.pokemonCard.rawValue)
                let pkmOffset = self.viewModel.pokemons.count + 1
                for skeleton in self.viewModel.skeletonPokemons.enumerated() {
                    if skeleton.offset + pkmOffset > c {
                        insertionPaths.append(IndexPath(item: c + skeleton.offset, section: HomepageSection.pokemonCard.rawValue))
                    }
                }
                self.collectionView.insertItems(at: insertionPaths)
            }
            self.collectionView.performBatchUpdates(sectionUpdates) { _ in
                self.collectionView.reloadItems(at: insertionPaths)
                self.viewModel.fetchPokemon {
                    let sectionUpdates1 = {
                        var paths = [IndexPath]()
                        let c = self.collectionView.numberOfItems(inSection: HomepageSection.pokemonCard.rawValue)
                        for i in 1...self.viewModel.maxSkeletonCells {
                            paths.append(IndexPath(item: c - i, section: HomepageSection.pokemonCard.rawValue))
                        }
                        self.collectionView.deleteItems(at: paths)
                        self.viewModel.onRemovedSkeleton {
                            var pathsToAdd = [IndexPath]()
                            for pokemon in self.viewModel.pokemons.enumerated() {
                                pathsToAdd.append(IndexPath(item: pokemon.offset, section: HomepageSection.pokemonCard.rawValue))
                            }
                            pathsToAdd = pathsToAdd.filter({ $0.item >= paths.last!.item })
                            self.collectionView.insertItems(at: pathsToAdd)
                        }
                    }
                    self.collectionView.performBatchUpdates(sectionUpdates1) { _ in
                        self.viewModel.onCompleted()
                    }
                }
            }
        }
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - HomepageViewController+UICollectionViewDataSource, UICollectionViewDelegate
extension HomepageViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections()
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.numberOfItemsInSection(section)
    }
    
    public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: CollectionGenericHeader.reuseIdentifier, for: indexPath)
            viewModel.configureSupplementaryView(sectionHeader, indexPath: indexPath)
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuseIdentifier = indexPath.row > viewModel.pokemons.count - 1 && indexPath.section == HomepageSection.pokemonCard.rawValue ? SkeletonCard.reuseIdentifier : viewModel.sectionReuseIdentifiers(indexPath: indexPath)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        viewModel.configureCell(cell: cell, indexPath: indexPath)
        return cell
    }
    
}

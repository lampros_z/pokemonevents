//
//  HomepageCoordinator.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import UIKit

public protocol HomepageCoordinatorProtocol {
    func start() -> UIViewController
}

public final class HomepageCoordinator: HomepageCoordinatorProtocol {
    private var navigationController: UINavigationController!
    
    private let homepageViewController: HomepageViewController

    
    init(homepageViewController: HomepageViewController) {
        self.homepageViewController = homepageViewController
    }

    public func start() -> UIViewController {
        navigationController = UINavigationController(rootViewController: homepageViewController)
        navigationController.navigationBar.tintColor = ColorPalette.contentAcid
        return navigationController
    }
    
}


extension HomepageCoordinator: HomepageCoordinatorDelegate {
    public func navigateToAuthenticationScreen() {
        navigationController.pushViewController(AuthenticationViewController(), animated: true)
    }
    
    public func navigateToConnectionScreen() {
        navigationController.pushViewController(ConnectionViewController(), animated: true)
    }
}

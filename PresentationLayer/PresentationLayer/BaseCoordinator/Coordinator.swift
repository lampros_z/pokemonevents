//
//  Coordinator.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import UIKit

public protocol Coordinator {
    func start() -> UIViewController
}

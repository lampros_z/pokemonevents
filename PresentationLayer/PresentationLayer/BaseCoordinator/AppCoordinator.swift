//
//  AppCoordinator.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import UIKit

public final class AppCoordinator: Coordinator {
    
    public init() {
        
    }
    
    public func start() -> UIViewController {
        let mainAppFlowAssembler = MainAppFlowAssembler()
        let mainCoordinator = mainAppFlowAssembler.build()
        return mainCoordinator.start()
    }
}

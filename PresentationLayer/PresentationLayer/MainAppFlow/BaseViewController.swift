//
//  BaseViewController.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import UIKit

public class BaseViewController: UIViewController {
    
    public let contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var contentViewConstraints: [NSLayoutConstraint] = [
        contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
        contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        contentView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
        contentView.bottomAnchor.constraint(equalTo: stickyLineView.topAnchor)
    ]
    
    private let stickyLineView: UIView = {
       let view = UIView()
        view.backgroundColor = ColorPalette.neutralsEightHundred
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var stickyLineViewConstaints: [NSLayoutConstraint] = [
        stickyLineView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        stickyLineView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
        stickyLineView.heightAnchor.constraint(equalToConstant: 2),
        stickyLineView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
    ]
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        configureAppearance()
        addChildViewsToHierarchy()
        setupLayout()
    }
    
    private func configureAppearance() {
        view.backgroundColor = ColorPalette.backgroundBlack
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = ColorPalette.backgroundBlack
        navigationController?.navigationBar.standardAppearance = appearance;
        navigationController?.navigationBar.scrollEdgeAppearance = navigationController?.navigationBar.standardAppearance
    }
    
    private func addChildViewsToHierarchy() {
        view.addSubview(contentView)
        view.addSubview(stickyLineView)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate( contentViewConstraints + stickyLineViewConstaints )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

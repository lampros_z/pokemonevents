//
//  MainAppFlowAssembler.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import UIKit
import Swinject

public protocol MainAppFlowAssemblerProtocol {
    func build() -> MainTabBarCoordinatorProtocol
}

public class MainAppFlowAssembler: MainAppFlowAssemblerProtocol {
    private let assembler: Assembler
    
    public init() {
        assembler = Assembler([MainAppFlowContainer()], parent: nil)
    }
    
    public func build() -> MainTabBarCoordinatorProtocol {
        assembler.resolver.resolve(MainTabBarCoordinatorProtocol.self)!
    }
    
}

public class MainAppFlowContainer: Assembly {
    
    public init() {
        
    }
    
    public func assemble(container: Container) {
        container.register(MainTabBarCoordinatorProtocol.self) { _ in
            let homepageCoordinator = HomepageFlowAssembler().build()
            let searchCoordinator = SearchFlowAssembler().build()
            let ticketsCoordinator = TicketsFlowAssembler().build()
            
            let homepageViewController = homepageCoordinator.start()
            homepageViewController.tabBarItem = UITabBarItem(
                title: GenericConstants.homeTabTitle,
                image: ImageCatalog.homeIcon,
                selectedImage: ImageCatalog.homeIcon
            )
            
            let searchViewController = searchCoordinator.start()
            searchViewController.tabBarItem = UITabBarItem(
                title: GenericConstants.searchTabTitle,
                image: ImageCatalog.searchIcon,
                selectedImage: ImageCatalog.searchIcon
            )
            
            let ticketsViewController = ticketsCoordinator.start()
            ticketsViewController.tabBarItem = UITabBarItem(
                title: GenericConstants.ticketsTabTitle,
                image: ImageCatalog.ticketsIcon,
                selectedImage: ImageCatalog.ticketsIcon
            )
            
            return MainTabBarCoordinator(tabbedViewControllers: [homepageViewController, searchViewController, ticketsViewController])
        }
    }
    
}

//
//  MainTabBarCoordinator.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import UIKit

public protocol MainTabBarCoordinatorProtocol {
    func start() -> UIViewController
}

public class MainTabBarCoordinator: MainTabBarCoordinatorProtocol {
    private let tabbedViewControllers: [UIViewController]
    
    public init(tabbedViewControllers: [UIViewController]) {
        self.tabbedViewControllers = tabbedViewControllers
    }
    
    public func start() -> UIViewController {
        return MainTabBarViewController(tabbedViewControllers: tabbedViewControllers)
    }
}

//
//  MainTabBarViewController.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import UIKit

public class MainTabBarViewController: UITabBarController {
    
    let tabbedViewControllers: [UIViewController]
    
    public init(tabbedViewControllers: [UIViewController]) {
        self.tabbedViewControllers = tabbedViewControllers
        super.init(nibName: nil, bundle: nil)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        configureAppearance()
        setTabbedViewControllers()
    }
    
    private func configureAppearance() {
        tabBar.isTranslucent = false
        tabBar.barTintColor = ColorPalette.backgroundBlack
        tabBar.tintColor = ColorPalette.contentAcid
        tabBar.backgroundColor = ColorPalette.backgroundBlack
        tabBar.unselectedItemTintColor = ColorPalette.contentLightGrey
        UITabBarItem.appearance().setTitleTextAttributes([.font: Fonts.sfproMediumEight], for: .normal)
    }
    
    private func setTabbedViewControllers() {
        viewControllers = tabbedViewControllers
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

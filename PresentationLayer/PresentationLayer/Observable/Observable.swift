//
//  Observable.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import UIKit

public class Observable<T> {
    private var listeners: [((T?) -> ())] = []
    var value: T? {
        didSet {
            listeners.forEach {
                $0(value)
            }
        }
    }
    
    public init(value: T?) {
        self.value = value
    }
    
    public func bind(_ listener: @escaping (T?) -> ()) {
        listener(value)
        listeners.append(listener)
    }
}

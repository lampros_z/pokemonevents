//
//  AuthenticationViewController.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 16/7/23.
//

import UIKit


public class AuthenticationViewController: BaseViewController {
    
    private let label: UILabel = {
        let label = UILabel()
        label.font = Fonts.sfproRegularEighteen
        label.textColor = .white
        label.text = "Sign-in/Register screen"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        addSubviewsToHierarchy()
        setupLayout()
    }
    
    private func addSubviewsToHierarchy() {
        contentView.addSubview(label)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }

}

//
//  SVGWebview.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import UIKit
import WebKit

public class SVGWebview: UIView, WKNavigationDelegate, SVGWebviewProtocol {
    private var isConfigured = false
    private var operationQueue = [String]()
    private lazy var injectionCallback: (String) -> () = { base64png in
        self.webView.evaluateJavaScript("loadSVG('\(base64png)')") { _, _ in }
    }
    
    private let webView: WKWebView = {
        let webView = WKWebView()
        webView.contentMode = .scaleAspectFit
        webView.scrollView.showsHorizontalScrollIndicator = false
        webView.scrollView.showsVerticalScrollIndicator = false
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.backgroundColor = .clear
        webView.isOpaque = false
        webView.scrollView.backgroundColor = .clear
        return webView
    }()
    
    private lazy var webviewConstraints: [NSLayoutConstraint] = [
        webView.leadingAnchor.constraint(equalTo: leadingAnchor),
        webView.topAnchor.constraint(equalTo: topAnchor),
        webView.trailingAnchor.constraint(equalTo: trailingAnchor),
        webView.bottomAnchor.constraint(equalTo: bottomAnchor)
    ]
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        configureAppearance()
        addSubviewsToHierarchy()
        setupLayout()
        subscribeToNavigationDelegate()
    }
    
    public func configure() {
        guard !isConfigured else { return }
        guard let path = Bundle.init(for: type(of: self)).path(forResource: "index", ofType: "html") else { return }
        let url = URL(fileURLWithPath: String(format: "%@", path))
        webView.load(URLRequest(url: url))
    }
    
    public func loadSVG(base64png: String) {
        if isConfigured {
            injectionCallback(base64png)
        } else {
            operationQueue.append(base64png)
        }
    }
    
    private func configureAppearance() {
        backgroundColor = .clear
    }
    
    private func addSubviewsToHierarchy() {
        addSubview(webView)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate(webviewConstraints)
    }
    
    private func subscribeToNavigationDelegate() {
        webView.navigationDelegate = self
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if !isConfigured, !operationQueue.isEmpty {
            injectionCallback(operationQueue.removeFirst())
            if !operationQueue.isEmpty {
                operationQueue.removeAll()
            }
        }
        isConfigured = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

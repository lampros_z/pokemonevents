//
//  SVGWebviewDelegate.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 15/7/23.
//

import Foundation

public protocol SVGWebviewProtocol: AnyObject {
    func configure()
    func loadSVG(base64png: String)
}

//
//  Spacers.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 13/7/23.
//

import UIKit

public class HorizontalSpacer: UIView {
    
    public init(width: CGFloat) {
        super.init(frame: .zero)
        backgroundColor = .clear
        setContentHuggingPriority(.defaultLow, for: .horizontal)
        setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(greaterThanOrEqualToConstant: width).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

public class VerticalSpacer: UIView {
    
    public init(height: CGFloat) {
        super.init(frame: .zero)
        backgroundColor = .clear
        setContentHuggingPriority(.defaultLow, for: .vertical)
        setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(greaterThanOrEqualToConstant: height).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

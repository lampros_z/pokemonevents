//
//  LocationView.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 13/7/23.
//

import UIKit

public class LocationView: UIView {
    private let locationImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = ImageCatalog.locationIcon
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var locationImageViewConstraints: [NSLayoutConstraint] = [
        locationImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
        locationImageView.topAnchor.constraint(equalTo: topAnchor),
        locationImageView.widthAnchor.constraint(equalToConstant: 32),
        locationImageView.heightAnchor.constraint(equalToConstant: 32)
    ]
    
    private let locationLabel: UILabel = {
        let label = UILabel()
        label.font = Fonts.sfproMediumEighteen
        label.textColor = .white
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        label.lineBreakStrategy = .standard
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var locationLabelConstraints: [NSLayoutConstraint] = [
        locationLabel.leadingAnchor.constraint(equalTo: locationImageView.trailingAnchor, constant: 4),
        locationLabel.bottomAnchor.constraint(equalTo: locationImageView.bottomAnchor),
        locationLabel.topAnchor.constraint(equalTo: locationImageView.topAnchor),
        locationLabel.trailingAnchor.constraint(equalTo: trailingAnchor)
    ]
    
    private let lineSeperator: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var lineSeperatorConstraints: [NSLayoutConstraint] = [
        lineSeperator.leadingAnchor.constraint(equalTo: locationLabel.leadingAnchor),
        lineSeperator.trailingAnchor.constraint(equalTo: locationLabel.trailingAnchor),
        lineSeperator.topAnchor.constraint(equalTo: locationLabel.bottomAnchor),
        lineSeperator.heightAnchor.constraint(equalToConstant: 2),
        lineSeperator.bottomAnchor.constraint(equalTo: bottomAnchor)
    ]
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        addChildViewsToHierarchy()
        setupLayout()
    }
    
    public func configureLoction(_ location: String) {
        locationLabel.text = location
    }
    
    private func addChildViewsToHierarchy() {
        addSubview(locationImageView)
        addSubview(locationLabel)
        addSubview(lineSeperator)
    }
    
    private func setupLayout() {
        locationLabel.setContentHuggingPriority(.defaultLow, for: .horizontal)
        locationLabel.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        NSLayoutConstraint.activate( locationImageViewConstraints + locationLabelConstraints + lineSeperatorConstraints )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

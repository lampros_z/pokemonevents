//
//  ImageCatalog.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import UIKit

public class ImageCatalog {
    public static let homeIcon = UIImage(named: "Home")
    public static let searchIcon = UIImage(named: "Search")
    public static let ticketsIcon = UIImage(named: "Ticket")
    public static let locationIcon = UIImage(named: "Location")
    public static let eventThumbnail = UIImage(named: "EventThumbnail")
}

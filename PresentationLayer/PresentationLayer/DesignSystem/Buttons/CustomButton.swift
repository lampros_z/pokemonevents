//
//  CustomButton.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 13/7/23.
//

import UIKit

public class CustomButton: UIButton {
    public init(
        title: String,
        font: UIFont,
        color: UIColor = ColorPalette.contentAcid,
        titleColor: UIColor = ColorPalette.backgroundBlack,
        cornerRadius: CGFloat = 4
    ) {
        super.init(frame: .zero)
        titleLabel?.font = font
        setTitle(title, for: .normal)
        setTitleColor(titleColor, for: .normal)
        backgroundColor = color
        layer.cornerRadius = cornerRadius

        contentEdgeInsets = UIEdgeInsets(
          top: 8,
          left: 16,
          bottom: 8,
          right: 16
        )
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

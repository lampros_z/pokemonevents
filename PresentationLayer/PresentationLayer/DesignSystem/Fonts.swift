//
//  Fonts.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import UIKit

public struct Fonts {
    public static let sfproRegularFourteen = UIFont.systemFont(ofSize: 14, weight: .regular)
    public static let sfproRegularEighteen = UIFont.systemFont(ofSize: 18, weight: .regular)
    public static let sfproRegularTwentyTwo = UIFont.systemFont(ofSize: 22, weight: .regular)
    public static let sfproMediumEight = UIFont.systemFont(ofSize: 8, weight: .medium)
    public static let sfproMediumSixteen = UIFont.systemFont(ofSize: 16, weight: .medium)
    public static let sfproMediumEighteen = UIFont.systemFont(ofSize: 18, weight: .medium)
    public static let sfproBoldTwelve = UIFont.systemFont(ofSize: 12, weight: .bold)
}

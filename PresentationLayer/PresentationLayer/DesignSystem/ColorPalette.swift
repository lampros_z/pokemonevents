//
//  ColorPalette.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import UIKit

public struct ColorPalette {
    
    public static let backgroundBlack = UIColor(red: 22.0 / 255.0, green: 22.0 / 255.0, blue: 22.0 / 255.0, alpha: 1)
    
    public static let backgroundSurfaceGrey = UIColor(red: 50.0 / 255.0, green: 50.0 / 255.0, blue: 50.0 / 255.0, alpha: 1)
    
    public static let contentAcid = UIColor(red: 221.0 / 255.0, green: 255.0 / 255.0, blue: 85.0 / 255.0, alpha: 1)
    
    public static let contentLightGrey = UIColor(red: 194.0 / 255.0, green: 194.0 / 255.0, blue: 194.0 / 255.0, alpha: 1)
    
    public static let neutralsEightHundred = UIColor(red: 34.0 / 255.0, green: 34.0 / 255.0, blue: 34.0 / 255.0, alpha: 1)
    
    public static let neutralsDefault = UIColor(red: 22.0 / 255.0, green: 22.0 / 255.0, blue: 22.0 / 255.0, alpha: 1)
}

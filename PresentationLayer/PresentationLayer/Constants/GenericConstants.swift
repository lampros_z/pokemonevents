//
//  GenericConstants.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import Foundation

public struct GenericConstants {
    public static let homeTabTitle = "HOME"
    public static let searchTabTitle = "SEARCH"
    public static let ticketsTabTitle = "TICKETS"
}

//
//  SearchFlowAssembler.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import Foundation
import Swinject

public protocol SearchFlowAssemblerProtocol {
    func build() -> SearchCoordinatorProtocol
}


public class SearchFlowAssembler: SearchFlowAssemblerProtocol {
    private let assembler: Assembler
    
    public init() {
        assembler = Assembler([SearchFlowContainer()], parent: nil)
    }
    
    public func build() -> SearchCoordinatorProtocol {
        assembler.resolver.resolve(SearchCoordinatorProtocol.self)!
    }
}

public class SearchFlowContainer: Assembly {
    
    public init() { }
    
    public func assemble(container: Container) {
        container.register(SearchCoordinatorProtocol.self) { r in
            let searchViewModel = r.resolve(SearchViewModel.self)!
            return SearchCoordinator(searchViewController: SearchViewController(viewModel: searchViewModel))
        }
        
        container.register(SearchViewModel.self) { _ in
            SearchViewModel()
        }
    }
    
}

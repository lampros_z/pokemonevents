//
//  SearchCoordinator.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 12/7/23.
//

import UIKit

public protocol SearchCoordinatorProtocol {
    func start() -> UIViewController
}

public final class SearchCoordinator: SearchCoordinatorProtocol {
    private let searchViewController: SearchViewController
    private var navigationController: UINavigationController!
    
    public init(searchViewController: SearchViewController) {
        self.searchViewController = searchViewController
    }
    
    public func start() -> UIViewController {
        navigationController = UINavigationController(rootViewController: searchViewController)
        return navigationController
    }
}

//
//  TicketsFlowAssembler.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 16/7/23.
//

import Foundation
import Swinject

public protocol TicketsFlowAssemblerProtocol {
    func build() -> TicketsCoordinatorProtocol
}


public class TicketsFlowAssembler: TicketsFlowAssemblerProtocol {
    private let assembler: Assembler
    
    public init() {
        assembler = Assembler([TicketsFlowContainer()], parent: nil)
    }
    
    public func build() -> TicketsCoordinatorProtocol {
        assembler.resolver.resolve(TicketsCoordinatorProtocol.self)!
    }
}

public class TicketsFlowContainer: Assembly {
    
    public init() { }
    
    public func assemble(container: Container) {
        container.register(TicketsCoordinatorProtocol.self) { r in
            let ticketsViewModel = r.resolve(TicketsViewModel.self)!
            return TicketsCoordinator(ticketsViewController: TicketsViewController(viewModel: ticketsViewModel))
        }
        
        container.register(TicketsViewModel.self) { _ in
            TicketsViewModel()
        }
    }
    
}

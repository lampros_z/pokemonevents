//
//  TicketsCoordinator.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 16/7/23.
//

import UIKit

public protocol TicketsCoordinatorProtocol {
    func start() -> UIViewController
}

public final class TicketsCoordinator: TicketsCoordinatorProtocol {
    private let ticketsViewController: TicketsViewController
    private var navigationController: UINavigationController!
    
    public init(ticketsViewController: TicketsViewController) {
        self.ticketsViewController = ticketsViewController
    }
    
    public func start() -> UIViewController {
        navigationController = UINavigationController(rootViewController: ticketsViewController)
        return navigationController
    }
}

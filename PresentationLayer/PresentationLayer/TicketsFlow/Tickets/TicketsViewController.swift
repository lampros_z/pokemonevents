//
//  TicketsViewController.swift
//  PresentationLayer
//
//  Created by Lampros Zouloumis on 16/7/23.
//

import UIKit

public class TicketsViewController: BaseViewController {
    private let viewModel: TicketsViewModel
    
    private let label: UILabel = {
        let label = UILabel()
        label.font = Fonts.sfproRegularEighteen
        label.textColor = .white
        label.text = "Tickets screen"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    public init(viewModel: TicketsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        addSubviewsToHierarchy()
        setupLayout()
    }
    
    private func addSubviewsToHierarchy() {
        contentView.addSubview(label)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
